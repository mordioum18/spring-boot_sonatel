pipeline {
  agent {
    label 'maven'
  }
  stages {
    stage('Build App') {
      steps {
        git branch: env.SOURCE_GIT_REF, url: env.SOURCE_GIT_REPOSITORY
        sh "mvn clean install -DskipTests=true"
      }
    }
    stage('Test') {
      steps {
        sh "mvn test"
        step([$class: 'JUnitResultArchiver', testResults: '**/target/surefire-reports/TEST-*.xml'])
      }
    }
    stage('Code Analysis') {
      steps {
        script {
          sh "mvn sonar:sonar -Dsonar.host.url=${env.URL_SONAR} -DskipTests=true"
        }
      }
    }
    //stage('Archive App') {
    //  steps {
    //    sh "mvn deploy -DskipTests=true -P nexus"
    //  }
    //}
    stage('Build Image') {
      steps {
        sh "cp target/spring-boot-demo-0.0.1-SNAPSHOT.jar target/ROOT.jar"
        script {
          openshift.withCluster() {
            openshift.withProject(env.TEST_PROJECT) {
              openshift.selector("bc", env.NAME_PROJECT).startBuild("--from-file=target/ROOT.jar", "--wait=true")
            }
          }
        }
      }
    }
    stage('Deploy TEST') {
      steps {
        script {
          openshift.withCluster() {
            openshift.withProject(env.TEST_PROJECT) {
              openshift.selector("dc", env.NAME_PROJECT).rollout().latest();
            }
          }
        }
      }
    }
    stage('Promote to PROD?') {
      steps {
        input message: "Promote to PROD?", ok: "Promote"
        script {
          openshift.withCluster() {
            openshift.tag("${env.TEST_PROJECT}/${env.NAME_PROJECT}:latest", "${env.PROD_PROJECT}/${env.NAME_PROJECT}:prod")
          }
        }
      }
    }
    stage('Deploy PROD') {
      steps {
        script {
          openshift.withCluster() {
            openshift.withProject(env.PROD_PROJECT) {
              openshift.selector("dc", env.NAME_PROJECT).rollout().latest();
            }
          }
        }
      }
    }
  }
}