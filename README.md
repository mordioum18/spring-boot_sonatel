SpringBoot - CentOS Docker image
========================================

Ce référentiel contient la source pour la construction de différentes versions d'applications springboot en tant qu’image Docker reproductible utilisant
[source-to-image] (https://github.com/openshift/source-to-image).
L'image résultante peut être exécutée à l'aide de [Docker] (http://docker.io).

Les versions
---------------
La version de CentOS actuellement fournie est:
* CentOS7

La versions Java actuellement fournie est:
* Openjdk-8

La version Maven actuellement fournie est:
* Maven-3.3.9

Installation
---------------

Cette image est disponible sur la registry de la Sonatel. Pour le télécharger, lancez:

`` `
$ docker pull registry.tools.orange-sonatel.com/spring-boot-maven3-jdk8:0.0.1
`` `

Usage
---------------------
Construire une application  simple `spring boot`
en utilisant [S2I] (https://github.com/openshift/source-to-image) puis exécutez 
l'image résultante avec la commande [Docker] (http://docker.io):

`` `
$ s2i build <Source de l'application spring boot>  spring-boot-maven3-jdk8:0.0.1 springboottest
$ docker run -p 8080: 8080 springboottest
`` `

** Accéder à l'application: 

`` `
$ curl 127.0.0.1:8080
`` `


Organisation du référentiel
------------------------
* **Dockerfile**

 Le Dockerfile installe tous les outils et les bibliothèques de construction nécessaires qui sont nécessaires pour construire et exécuter une application Java Spring Boot dans un serveur Web intégré comme Apache Tomcat. L'exécution d'une construction de docker produira une image  S2I pouvant être déployée sur OpenShift et utilisée pour créer une application Spring Boot.

* **`s2i/bin/`**

 Ce dossier contient des scripts exécutés par [S2I] (https://github.com/openshift/source-to-image):

*  **assemble**

 Ce script construit l'application en utilisant Maven


*   **run**

 Ce script est responsable de l'exécution de l'application.

* **save-artefacts**

  Ce script enregistre les artefacts construits

* **`contrib/`**

  Ce dossier peut être utilisé pour injecter un configuration de Maven spécifique (setting.xml)
  

Utilisation de l’image S2I d’application Spring Boot dans OpenShift
---------------

1. Utilisez la commande ci-dessous pour créer l'image S2I et l'enregistrer dans le registre de docker intégré. 

`` `
oc  oc new-build --strategy=docker --name=springboot-java-maven -e NAMESPACE=<votre namespace> http://git.tools.orange-sonatel.com/scm/mal/malaw-spring-boot.git
`` `
  **Vous pouvez aussi charger le template template/spring-boot-maven-builder.yaml et démarez le build à partir de la console web**
  
2. Téléchargez le template template/template.yaml à partir de ce référentiel et enregistrez-le sur votre ordinateur local. Utilisez ensuite la commande ci-dessous pour télécharger le template dans votre projet.

`` `
oc create -f template/spring-boot-maven-app.yaml
`` `

3. Cliquez sur "Ajouter au projet" dans la console Web OpenShift CP pour créer une nouvelle application, puis sélectionnez le template "springboot-java-maven-app". Vous serez alors présenté avec un formulaire où vous pouvez spécifier

* APP_NAME: Nom de l'application
* GIT_URI: Référenciel Git. 
* GIT_REF: Git branch/tag reference. Sa valeur par défaut est master
* CONTEXT_DIR: Sous dossier contenant le projet. 